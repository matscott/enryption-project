$( document ).ready(function() {
    $("#encrypt-button").click(function() {
        var plaintext = $('#message').val();
        var encodedPaintext = encode(plaintext);
        var encryptedText = encrypt(encodedPaintext);
        $('#cipher').html(encryptedText.join(' '));
    });
});

$( document ).ready(function() {
    $('#decrypt-button').click(function() {
        var cipher = $('#cipher-text').val().split(' ');
        var decryptedText = decrypt(cipher);
        $('#plaintext').html(decode(decryptedText));
    });
});



var rsa = {
  p: 7,
  q: 11,
  n: 77,
  e: 17,
  d: 53
}

var lookupTable = {
  0: ' ',
  1: 'a',
  2: 'b',
  3: 'c',
  4: 'd',
  5: 'e',
  6: 'f',
  7: 'g',
  8: 'h',
  9: 'i',
  10: 'j',
  11: 'k',
  12: 'l',
  13: 'm',
  14: 'n',
  15: 'o',
  16: 'p',
  17: 'q',
  18: 'r',
  19: 's',
  20: 't',
  21: 'u',
  22: 'v',
  23: 'w',
  24: 'x',
  25: 'y',
  26: 'z'
}

function encrypt(plainTextArray) {
    var encryptionArray = [];
    for (var number in plainTextArray) {
        number = new BigNumber(plainTextArray[number]);
        number = number.toPower(rsa.e).modulo(rsa.n);
        encryptionArray.push(number);
    }
    return encryptionArray;
}

function decrypt(encryptedArray) {
    var decryptedArray = [];
    for (var number in encryptedArray) {
        number = new BigNumber(encryptedArray[number]);
        number = number.toPower(rsa.d).modulo(rsa.n);
        decryptedArray.push(number);
    }
  return decryptedArray;
}

function decode(encodedResult) {
  var result = '';
  for (number in encodedResult) {
    result += lookupTable[encodedResult[number]];
  }
  return result;
}

function encode(plaintext) {
    var encodedArray = [];
    for (var i = 0, len = plaintext.length; i < len; i++) {
      for(var key in lookupTable) {
            if(lookupTable[key] === plaintext[i]) {
                encodedArray.push(key);
            }
        }
    }
    console.log(encodedArray);
    return encodedArray;
}

